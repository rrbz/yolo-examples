# Yolo v3 setup script and examples

This is some prepared and fresh stuff to get ready
and work on YOLO, the NN for real-time object detecion.

Simply execute `./setup.sh` to setup the environment
and be ready to work with yolo.


## Directories schema

The script will generate a few directories:

- `cfg`: Yolo configuration
- `data`: coco.names file ("names of the things")
- `demo`: sample Python scripts
- `env`: Python environment
- `samples`: sample images
- `weights`: weights

