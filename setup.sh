#!/bin/bash

# update APT cache
sudo apt update

# install dependencies
sudo apt install -y python3-dev python3-numpy python3-pip cython3 whiptail

# check if virtualenv has already been installed (maybe via PyPI)
if [ -z "$(which virtualenv)" ] ; then
	sudo apt install -y virtualenv python3-virtualenv
fi

# check if a Python environment is already there
if [ ! -e env ] ; then
	virtualenv --python=python3 env
fi

# checkout into the environment
. env/bin/activate

# if you need to compile it from scracth, comment
# out the following line and follow
# https://github.com/madhawav/YOLO3-4-Py/blob/master/tools/install_opencv34.sh
pip3 install opencv-python

# install yolo34py
if whiptail --title "YOLO setup" --defaultno --yesno "Do you want to install the GPU-accelerated version of YOLO?" 10 50 ; then
	whiptail --title "YOLO setup" --msgbox "This installer assume you already installed nVidia proprietary driver v. 440.40, CUDA v. 10.2, libcudnn7 and libcudnn7-dev or greater." 20 50
	sudo apt install libopencv-dev
	pip3 install yolo34py-gpu
else
	pip3 install yolo34py
fi

# download models using their own script (if required)
if [ -e cfg ] || [ -e data ] || [ -e weights ]; then
	if whiptail --title "YOLO setup" --defaultno --yesno "YOLO confgiuration and models are already there, do you want to update them?" 10 50 ; then
		./download_models.sh
	fi
else
	if whiptail --title "YOLO setup" --yesno "YOLO needs about 300MB of models and configurations to download, do you want to continue?" 10 50 ; then
		./download_models.sh
	fi
fi

# download sample images
mkdir -p samples
cd samples

for url in \
	"http://absfreepic.com/absolutely_free_photos/original_photos/playing-elephants-4200x2649_67248.jpg" \
	"http://absfreepic.com/absolutely_free_photos/original_photos/crowded-cars-on-street-4032x2272_48736.jpg" \
	"http://absfreepic.com/absolutely_free_photos/original_photos/the-taxi-on-street-3264x2448_44599.jpg" \
	"http://absfreepic.com/absolutely_free_photos/original_photos/cars-on-busy-street-3456x2304_73674.jpg"; do
	if [ ! -e "$(echo $url | rev | cut -d / -f 1 | rev)" ] ; then
		wget --progress=dot:mega $url
	fi
done

cd ..


# download python examples
mkdir -p demo
cd demo

for url in "https://raw.githubusercontent.com/madhawav/YOLO3-4-Py/master/image_demo.py" \
	"https://raw.githubusercontent.com/madhawav/YOLO3-4-Py/master/video_demo.py" \
	"https://raw.githubusercontent.com/madhawav/YOLO3-4-Py/master/webcam_demo.py"; do
	if [ ! -e "$(echo $url | rev | cut -d / -f 1 | rev)" ] ; then
		wget --progress=dot:mega $url
	fi
done

sed -i 's+import os$+import os, sys+g' image_demo.py
sed -i 's+os\.path\.join(os\.environ\["DARKNET_HOME"\],"data/dog\.jpg")+sys\.argv\[-1\]+g' image_demo.py

cd ..
